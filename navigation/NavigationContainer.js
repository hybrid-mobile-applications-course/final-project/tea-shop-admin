import React, {useEffect, useRef} from 'react';
import {useSelector} from 'react-redux';
import {NavigationActions} from 'react-navigation';

import PrimaryNavigator from './PrimaryNavigator';

const NavigationContainer = props => {
  const navRef = useRef();
  const isAuthenticated = useSelector(state => !!state.authentication.token);

  useEffect(() => {
    if (!isAuthenticated) {
      navRef.current.dispatch(NavigationActions.navigate({routeName: 'Auth'}));
    }
  }, [isAuthenticated]);

  return <PrimaryNavigator ref={navRef}/>;
};

export default NavigationContainer;
