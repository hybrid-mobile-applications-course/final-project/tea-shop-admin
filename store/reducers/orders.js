import {SET_ORDERS, UPDATE_ORDER} from '../actions/orders';
import Order from '../../models/order';

const initialState = {
  orders: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_ORDERS:
      return {
        orders: action.orders,
      };
    case UPDATE_ORDER:
      const ordersIndex = state.orders.findIndex(
          prod => prod.id === action.pid);
      const updatedOrder = new Order(
          action.pid,
          state.userProducts[ordersIndex].ownerId,
          action.orderData.status,
          action.orderData.items,
          action.orderData.amount,
          action.orderData.date,
      );
      const updatedOrders = [...state.orders];
      updatedOrders[ordersIndex] = updatedOrder;
      return {
        ...state,
        orders: updatedOrders,
      };

  }

  return state;
};
