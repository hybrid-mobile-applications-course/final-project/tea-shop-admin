import Order from '../../models/order';

import config from '../../config';

export const SET_ORDERS = 'SET_ORDERS';
export const UPDATE_ORDER = 'UPDATE_ORDER';

export const fetchOrders = () => {
  return async (dispatch) => {
    try {
      const response = await fetch(
          `${config.realtimeDatabase}orders.json`,
      );

      if (!response.ok) {
        throw new Error('Something went wrong!');
      }

      const resData = await response.json();
      const loadedOrders = [];
      for (const key in resData) {
        loadedOrders.push(
            new Order(
                key,
                resData[key].status,
                resData[key].cartItems,
                resData[key].totalAmount,
                key,
                new Date(resData[key].date),
                resData[key].address,
                resData[key].latitude,
                resData[key].longitude

            ),
        );
      }
      dispatch({type: SET_ORDERS, orders: loadedOrders});
    } catch (err) {
      throw err;
    }
  };
};

export const updateOrder = (id) => {
  return async (dispatch, getState) => {
    const token = getState().authentication.token;
    const status = 'Ready';
    const response = await fetch(
        `${config.realtimeDatabase}orders/${id}.json?auth=${token}`,
        {
          method: 'PATCH',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            status,
          }),
        });

    if (!response.ok) {
      throw new Error('Something went wrong!');
    }

    dispatch({
      type: UPDATE_ORDER,
      pid: id,
      orderData: {
        status: status,
      },
    });
  };
};
