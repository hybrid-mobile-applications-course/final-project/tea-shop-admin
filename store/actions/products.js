import Product from '../../models/product';

import config from '../../config';

export const DELETE_PRODUCT = 'DELETE_PRODUCT';
export const CREATE_PRODUCT = 'CREATE_PRODUCT';
export const UPDATE_PRODUCT = 'UPDATE_PRODUCT';
export const SET_PRODUCTS = 'SET_PRODUCTS';

export const fetchProducts = () => {
  return async (dispatch, getState) => {
    try {
      const userId = getState().authentication.userId;
      const token = getState().authentication.token;

      const response = await fetch(
          `${config.realtimeDatabase}listings.json?auth=${token}`);
      if (!response.ok) {
        throw new Error('Something went wrong');
      }

      const responseData = await response.json();
      const loadedProducts = [];
      for (const key in responseData) {
        loadedProducts.push(new Product(
            key,
            responseData[key].ownerId,
            responseData[key].title,
            responseData[key].imageUrl,
            responseData[key].description,
            responseData[key].price,
            responseData[key].phone,
        ));
      }
      dispatch({
        type: SET_PRODUCTS,
        availableProducts: loadedProducts,
        userProducts: loadedProducts.filter(
            product => product.ownerId === userId,
        ),
      });
    } catch (err) {
      throw err;
    }
  };
};

export const createProduct = (title, description, imageUrl, price) => {
  return async (dispatch, getState) => {
    const userId = getState().authentication.userId;
    const token = getState().authentication.token;
    const displayName = getState().authentication.displayName;
    const response = await fetch(
        `${config.realtimeDatabase}listings.json?auth=${token}`,
        {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            title,
            description,
            imageUrl,
            price,
            ownerId: userId,
            phone: displayName,
          }),
        });

    if (!response.ok) {
      throw new Error('Something went wrong');
    }

    const responseData = await response.json();

    dispatch({
      type: CREATE_PRODUCT,
      productData: {
        id: responseData.name,
        title,
        description,
        imageUrl,
        price,
        ownerId: userId,
        phone: displayName,
      },
    });
  };
};

export const updateProduct = (id, title, description, imageUrl, price) => {
  return async (dispatch, getState) => {
    const token = getState().authentication.token;
    const response = await fetch(
        `${config.realtimeDatabase}listings/${id}.json?auth=${token}`,
        {
          method: 'PATCH',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            title,
            description,
            imageUrl,
            price,
          }),
        });

    if (!response.ok) {
      throw new Error('Something went wrong');
    }

    dispatch({
      type: UPDATE_PRODUCT,
      pid: id,
      productData: {
        title,
        description,
        imageUrl,
        price,
      },
    });
  };
};

export const deleteProduct = productId => {
  return async (dispatch, getState) => {
    const token = getState().authentication.token;
    const response = await fetch(
        `${config.realtimeDatabase}listings/${productId}.json?auth=${token}`,
        {
          method: 'DELETE',
        });

    if (!response.ok) {
      throw new Error('Something went wrong');
    }

    dispatch({
      type: DELETE_PRODUCT,
      pid: productId,
    });
  };
};
