import React, {useCallback, useState} from 'react';
import {ActivityIndicator, Button, StyleSheet, Text, View} from 'react-native';

import CartItem from './CartItem';
import Colors from '../../constants/Colors';
import * as ordersActions from '../../store/actions/orders';
import {useDispatch} from 'react-redux';
import Card from '../UI/Card';

const OrderItem = props => {

  const [showDetails, setShowDetails] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const dispatch = useDispatch();

  const submitHandler = useCallback(async () => {
    setIsLoading(true);
    try {
      await dispatch(
          ordersActions.updateOrder(
              props.id));
      props.navigation.goBack();
    } catch (e) {
    }
    setIsLoading(false);
  }, [dispatch]);

  if (isLoading) {
    return (
        <View style={styles.center}>
          <ActivityIndicator size='large' color={Colors.primary}/>
        </View>
    );
  }

  return (
      <Card style={styles.orderItem}>

        <View style={styles.statusView}>
          <Text style={styles.status}>{props.status}</Text>
        </View>
        <View style={styles.viewAddress}>
          <Text style={styles.address}>{props.address}</Text>
        </View>
        <View style={styles.summary}>
          <Text style={styles.totalAmount}>${props.amount.toFixed(2)}</Text>
          <Text style={styles.date}>{props.date}</Text>
        </View>


        <View style={styles.buttonsView}>
          <Button
              color={Colors.primary}
              title={showDetails ? 'Hide Details' : 'Show Details'}
              onPress={() => {
                setShowDetails(prevState => !prevState);
              }}
          />
          <Button
              color={Colors.primary}
              title={'Confirm order'}
              onPress={
                submitHandler
              }
          />
        </View>
        {showDetails && (
            <View style={styles.detailItems}>
              {props.items.map(cartItem => (
                  <CartItem
                      quantity={cartItem.quantity}
                      amount={cartItem.sum}
                      title={cartItem.productTitle}
                  />
              ))}
            </View>
        )}
      </Card>
  );
};

const styles = StyleSheet.create({
  orderItem: {
    margin: 20,
    padding: 10,
    alignItems: 'center',
  },
  buttonsView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '80%',
  },
  statusView: {
    width: '100%',
    marginBottom: 5,
  },
  viewAddress: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '100%',
    marginBottom: 15,
  },
  address: {
    fontSize: 16,
    fontFamily: 'open-sans',
    color: '#888',
  },
  status: {
    fontSize: 16,
    fontFamily: 'open-sans-bold',
    color: '#888',
  },
  summary: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '100%',
    marginBottom: 15,
  },
  totalAmount: {
    fontFamily: 'open-sans-bold',
    fontSize: 16,
  },
  date: {
    fontSize: 16,
    fontFamily: 'open-sans',
    color: '#888',
  },
  detailItems: {
    width: '100%',
  },
});

export default OrderItem;
