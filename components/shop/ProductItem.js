import React from 'react';
import {
  Image,
  Platform,
  StyleSheet,
  Text,
  TouchableNativeFeedback,
  TouchableOpacity,
  View,
} from 'react-native';

const ProductItem = props => {
  let TouchableComponent = TouchableOpacity;
  if (Platform.OS === 'android') {
    TouchableComponent = TouchableNativeFeedback;
  }
  return (
      <View style={styles.product}>
        <TouchableComponent onPress={props.onSelect} useForeground>
          <View>
            <Image style={styles.image} source={{uri: props.imageUrl}}/>
            <View style={styles.viewTexts}>
              <Text style={styles.title}>{props.title}</Text>
              <Text style={styles.price}>{props.price.toFixed(2)}€</Text>
            </View>
            <View style={styles.viewButton}>
              {props.children}
            </View>
          </View>
        </TouchableComponent>
      </View>
  );
};

const styles = StyleSheet.create({

  product: {
    shadowColor: 'black',
    shadowOpacity: 0.26,
    shadowOffset: {width: 0, height: 2},
    shadowRadius: 8,
    elevation: 5,
    borderRadius: 10,
    backgroundColor: 'white',
    height: 300,
    margin: 20,
  },
  image: {
    width: '100%',
    height: '60%',
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
  },
  viewTexts: {
    alignItems: 'center',
    height: '20%',
    paddingTop: 10,
  },
  title: {
    fontFamily: 'open-sans-bold',
    fontSize: 18,
    marginVertical: 3,
  },
  price: {
    fontFamily: 'open-sans',
    fontSize: 14,
    color: '#888',
  },
  viewButton: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: '20%',
    paddingTop: 10,
    paddingHorizontal: 80,
    //width: '40%',
  },
});

export default ProductItem;
