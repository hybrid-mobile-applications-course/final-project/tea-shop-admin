import React, {useState} from 'react';
import {
  Alert,
  Button,
  Image,
  Platform,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import * as ImageSelector from 'expo-image-picker';
import * as Permissions from 'expo-permissions';

import config from '../../config';
import firebase from 'firebase/app';
import 'firebase/storage';

import Colors from '../../constants/Colors';

const PhotoSelector = props => {
  const [selectedPhoto, setSelectedPhoto] = useState();

  const firebaseConfig = {
    apiKey: config.apiKey,
    authDomain: config.authDomain,
    databaseURL: config.realtimeDatabase,
    projectId: config.projectId,
    storageBucket: config.storageBucket,
    messagingSenderId: config.messagingSenderId,
    appId: config.appId,
    measurementId: config.messagingSenderId,
  };

  if (!firebase.apps.length) {
    firebase.initializeApp(firebaseConfig);
  }

  const grantPermissions = async () => {
    const iOSResponse = await Permissions.askAsync(
        Permissions.CAMERA,
        Permissions.CAMERA_ROLL,
    );
    const androidResponse = await Permissions.askAsync(Permissions.CAMERA);
    if (Platform.OS === 'ios') {
      if (iOSResponse.status !== 'granted') {
        Alert.alert(
            'Permission not granted',
            'Please grant camera permission to continue using this app.',
            [{text: 'OK'}],
        );
        return false;
      }
    }
    if (Platform.OS === 'android') {
      if (androidResponse.status !== 'granted') {
        Alert.alert(
            'Permission denied',
            'Please grant camera permission to continue using this app.',
            [{text: 'OK'}],
        );
        return false;
      }
    }
    return true;
  };
  const openCameraHandler = async () => {
    const hasPermission = await grantPermissions();
    if (!hasPermission) {
      return;
    }
    const takenPhoto = await ImageSelector.launchCameraAsync({
      //allowsEditing: true,
      aspect: [16, 9],
      quality: 0.1,
    });

    let localUri = takenPhoto.uri;
    let filename = localUri.split('/').pop();
    let url = await uploadImageToFirebase(localUri, filename);

    setSelectedPhoto(takenPhoto.uri);
    props.onPhotoTaken(url);
  };

  return (
      <View style={styles.imageSelector}>
        <View style={styles.imagePreview}>
          {!selectedPhoto ? (
              <Text>Photo preview</Text>
          ) : (
              <Image style={styles.image} source={{uri: selectedPhoto}}/>
          )}
        </View>
        <Button
            title="Take a photo"
            color={Colors.primary}
            onPress={openCameraHandler}
        />
      </View>
  );
};

const styles = StyleSheet.create({
  imageSelector: {
    alignItems: 'center',
    marginBottom: 12,
  },
  imagePreview: {
    width: '100%',
    height: 200,
    marginBottom: 10,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: Colors.grey,
    borderWidth: 1,
  },
  image: {
    width: '100%',
    height: '100%',
  },
});

export const uploadImageToFirebase = async (uri, filename) => {

  const blob = await new Promise((resolve, reject) => {
    const xhr = new XMLHttpRequest();
    xhr.onload = () => {
      resolve(xhr.response);
    };
    xhr.responseType = 'blob';
    xhr.open('GET', uri, true);
    xhr.send(null);
  });

  const ref = firebase.storage().ref().child(`images/` + filename);

  let snapshot = await ref.put(blob);

  return await snapshot.ref.getDownloadURL();
};

export default PhotoSelector;
