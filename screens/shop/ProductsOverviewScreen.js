import React, {useCallback, useEffect, useState} from 'react';
import {
  ActivityIndicator,
  Alert,
  Button,
  FlatList,
  Platform,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {HeaderButtons, Item} from 'react-navigation-header-buttons';

import HeaderButton from '../../components/UI/HeaderButton';
import ProductItem from '../../components/shop/ProductItem';
import * as productsActions from '../../store/actions/products';
import Colors from '../../constants/Colors';

const ProductsOverviewScreen = props => {
  const [isLoading, setIsLoading] = useState(false);
  const [isRefreshing, setIsRefreshing] = useState(false);
  const [error, setError] = useState();
  const products = useSelector(state => state.products.availableProducts);
  const dispatch = useDispatch();

  const loadProducts = useCallback(async () => {
    setError(null);
    setIsRefreshing(true);
    try {
      await dispatch(productsActions.fetchProducts());
    } catch (err) {
      setError(err.message);
    }
    setIsRefreshing(false);
  }, [dispatch, setIsLoading, setError]);

  useEffect(() => {
    setIsLoading(true);
    loadProducts().then(() => {
      setIsLoading(false);
    });
  }, [dispatch, loadProducts]);

  useEffect(() => {
    const willFocusSubscription = props.navigation.addListener(
        'willFocus',
        () => {
          loadProducts();
          return () => {
            willFocusSubscription.remove();
          };
        },
        [loadProducts],
    );
  });

  const selectItemHandler = (id, title, phone) => {
    props.navigation.navigate('ProductDetail', {
      productId: id,
      productTitle: title,
      phone: phone,
    });
  };

  if (error) {
    return (
        <View style={styles.center}>
          <Text>
            Error, can't connect to database.
          </Text>
          <Button
              title="Retry"
              onPress={loadProducts}
              color={Colors.primary}/>
        </View>
    );
  }

  if (isLoading) {
    return (
        <View style={styles.center}>
          <ActivityIndicator size='large' color={Colors.primary}/>
        </View>
    );
  }

  if (!isLoading && products.length === 0) {
    return (
        <View style={styles.center}>
          <Text>
            No active listings.
          </Text>
        </View>
    );
  }

  const editProductHandler = (id) => {
    props.navigation.navigate('EditProduct', {productId: id});
  };

  const deleteHandler = (id) => {
    Alert.alert('Confirm deletion', 'Item will be deleted. Proceed?', [
      {text: 'Cancel', style: 'default'},
      {
        text: 'Confirm', style: 'destructive', onPress: () => {
          setIsLoading(true);
          dispatch(productsActions.deleteProduct(id));
          setIsLoading(false);
        },
      },
    ]);
  };

  return (
      <FlatList
          onRefresh={loadProducts}
          refreshing={isRefreshing}
          data={products}
          renderItem={
            itemData => <ProductItem
                imageUrl={itemData.item.imageUrl}
                title={itemData.item.title}
                price={itemData.item.price}
                onSelect={() => {
                  selectItemHandler(
                      itemData.item.id,
                      itemData.item.title);
                }}
            >
              <Button color={Colors.primary} title='Edit'
                      onPress={() => {
                        editProductHandler(itemData.item.id);
                      }}/>
              <Button color={Colors.primary}
                      title='Delete'
                      onPress={
                        deleteHandler.bind(
                            this,
                            itemData.item.id,
                        )}/>
            </ProductItem>
          }
      />
  );
};

ProductsOverviewScreen.navigationOptions = navData => {
  return {
    headerTitle: 'Stock',
    headerLeft: () => (
        <HeaderButtons HeaderButtonComponent={HeaderButton}>
          <Item
              title="Menu"
              iconName={Platform.OS === 'android' ? 'md-menu' : 'ios-menu'}
              onPress={() => {
                navData.navigation.toggleDrawer();
              }}
          />
        </HeaderButtons>
    ),
    headerRight: () => (
        <HeaderButtons HeaderButtonComponent={HeaderButton}>
          <Item
              title="Add"
              iconName={Platform.OS === 'android' ? 'md-create' : 'ios-create'}
              onPress={() => {
                navData.navigation.navigate('EditProduct');
              }}
          />
        </HeaderButtons>
    ),
  };
};

const styles = StyleSheet.create({
  center: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default ProductsOverviewScreen;
