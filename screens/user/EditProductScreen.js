import React, {useCallback, useEffect, useReducer, useState} from 'react';
import {
  ActivityIndicator,
  Alert,
  KeyboardAvoidingView,
  Platform,
  ScrollView,
  StyleSheet,
  View,
} from 'react-native';
import {HeaderButtons, Item} from 'react-navigation-header-buttons';
import {useDispatch, useSelector} from 'react-redux';

import Input from '../../components/UI/Input';
import ImageSelector from '../../components/UI/ImageSelector';
import HeaderButton from '../../components/UI/HeaderButton';
import Colors from '../../constants/Colors';
import * as productsActions from '../../store/actions/products';

const FORM_INPUT_UPDATE = 'FORM_INPUT_UPDATE';

const formReducer = (state, action) => {
  if (action.type === FORM_INPUT_UPDATE) {
    const updatedValues = {
      ...state.inputValues,
      [action.input]: action.value,
    };
    const updatedValidities = {
      ...state.inputValidities,
      [action.input]: action.isValid,
    };
    let updatedFormIsValid = true;
    for (const key in updatedValidities) {
      updatedFormIsValid = updatedFormIsValid && updatedValidities[key];
    }
    return {
      formIsValid: updatedFormIsValid,
      inputValidities: updatedValidities,
      inputValues: updatedValues,
    };
  }
  return state;
};

const EditProductScreen = props => {
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState();
  const prodId = props.navigation.getParam('productId');
  const editedProduct = useSelector(
      state => state.products.userProducts.find(prod => prod.id === prodId));

  const dispatch = useDispatch();

  const photoTakenHandler = photoPath => {
    formState.inputValues.imageUrl = photoPath;
  };

  const [formState, dispatchFormState] = useReducer(formReducer, {
    inputValues: {
      title: editedProduct ? editedProduct.title : '',
      imageUrl: editedProduct ? editedProduct.imageUrl : '',
      description: editedProduct ? editedProduct.description : '',
      price: editedProduct ? editedProduct.price : '',
    },
    inputValidities: {
      title: !!editedProduct,
      description: !!editedProduct,
      price: !!editedProduct,
    },
    formIsValid: !!editedProduct,
  });

  useEffect(() => {
    if (error) {
      Alert.alert('Error', error, [{text: 'OK'}]);
    }
  }, [error]);

  const submitHandler = useCallback(async () => {
    if (!formState.formIsValid) {
      Alert.alert('Invalid input', 'Please fix input errors!', [{text: 'OK'}]);
      return;
    } else if (formState.inputValues.imageUrl === '') {
      Alert.alert('No photo', 'Please take a photo of product', [{text: 'OK'}]);
      return;
    }
    setIsLoading(true);
    setError(null);
    try {
      if (editedProduct) {
        await dispatch(
            productsActions.updateProduct(
                prodId,
                formState.inputValues.title,
                formState.inputValues.description,
                formState.inputValues.imageUrl,
                +formState.inputValues.price));
      } else {
        await dispatch(
            productsActions.createProduct(
                formState.inputValues.title,
                formState.inputValues.description,
                formState.inputValues.imageUrl,
                +formState.inputValues.price));
      }
      props.navigation.goBack();
    } catch (err) {
      setError(err.message);
    }
    setIsLoading(false);
  }, [dispatch, prodId, formState]);

  useEffect(() => {
    props.navigation.setParams({
      'submit': submitHandler,
    });
  }, [submitHandler]);

  const inputChangeHandler = useCallback(
      (inputIdentifier, inputValue, inputValidity) => {
        dispatchFormState({
          type: FORM_INPUT_UPDATE,
          value: inputValue,
          isValid: inputValidity,
          input: inputIdentifier,
        });
      }, [dispatchFormState]);

  if (isLoading) {
    return (
        <View style={styles.center}>
          <ActivityIndicator size='large' color={Colors.primary}/>
        </View>
    );
  }

  return (
      <KeyboardAvoidingView
          style={{flex: 1}}
          keyboardVerticalOffset={20}>
        <ScrollView>
          <View style={styles.form}>
            <Input
                id='title'
                label='Title'
                errorText='Enter name of item you want to sell.'
                keyboardType='default'
                autocorrect
                autocapitalize='sentences'
                onInputChange={inputChangeHandler}
                initialValue={editedProduct ? editedProduct.title : ''}
                initialValidity={!!editedProduct}
                required
                minLength={2}
            />
            <Input
                id='price'
                label='Price'
                errorText='Enter price'
                keyboardType='decimal-pad'
                onInputChange={inputChangeHandler}
                initialValue={editedProduct
                    ? editedProduct.price.toString()
                    : ''}
                initialValidity={!!editedProduct}
                required
            />
            <Input
                id='description'
                label='Description'
                errorText='Describe the item you are selling.'
                keyboardType='default'
                autocorrect
                autocapitalize='sentences'
                multiline
                numberOfLines={5}
                onInputChange={inputChangeHandler}
                initialValue={editedProduct ? editedProduct.description : ''}
                initialValidity={!!editedProduct}
                required
                minLength={4}
            />
            <ImageSelector
                onPhotoTaken={photoTakenHandler}
            />
          </View>
        </ScrollView>
      </KeyboardAvoidingView>
  );
};

EditProductScreen.navigationOptions = navData => {
  const submitFunction = navData.navigation.getParam('submit');
  return {
    headerTitle: navData.navigation.getParam('productId')
        ? 'Edit Item'
        : 'Add Item',
    headerRight: () => (
        <HeaderButtons HeaderButtonComponent={HeaderButton}>
          <Item
              title="Save"
              iconName={Platform.OS === 'android'
                  ? 'md-checkmark'
                  : 'ios-checkmark'}
              onPress={submitFunction}
          />
        </HeaderButtons>
    ),
  };
};

const styles = StyleSheet.create({
  form: {
    margin: 20,
  },
  center: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default EditProductScreen;
