import moment from 'moment';

class Order {
  constructor(
      id, status, items, totalAmount, name, date, address, latitude,
      longitude) {
    this.id = id;
    this.status = status;
    this.items = items;
    this.totalAmount = totalAmount;
    this.name = name;
    this.date = date;
    this.address = address;
    this.latitude = latitude;
    this.longitude = longitude;
  }

  get readableDate() {
    return moment(this.date).format('MMMM Do YYYY, hh:mm');
  }
}

export default Order;
